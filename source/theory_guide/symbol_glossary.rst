.. _symbol-glossary:

Glossary of Symbols
-------------------

:math:`H` -enthalpy [kJ mol\ :math:`^{-1}`]

:math:`k` -intrinsic permeability [m\ :math:`^2`]

:math:`k_r` -relative permeability [-]

:math:`m` -van Genuchten parameter [-]

:math:`n` -van Genuchten parameter [-]

:math:`P` -pressure [Pa]

:math:`p_c` -capillary pressure [Pa]

:math:`p_c^0` -van Genuchten parameter [Pa]

:math:`P_{\text{sat}}` -saturated vapor pressure [Pa]

:math:`P_{cgl}` -liquid-gas capillary pressure [Pa]

:math:`{\boldsymbol{g}}` -gravity [m s\ :math:`^{-2}`]

:math:`Q_w` -source/sink [kmol m\ :math:`^{-3}` s\ :math:`^{-1}`]

:math:`{\boldsymbol{q}}` -Darcy flux [m s\ :math:`^{-1}`]

:math:`R` -gas constant [kJ K\ :math:`^{-1}` kmol\ :math:`^{-1}`]

:math:`s` -saturation [m\ :math:`^3` m\ :math:`^{-3}`]

:math:`s_e` -effective saturation [-]

:math:`s_r` -residual saturation [-]

:math:`s_0` -maximum saturation [-]

:math:`T` -temperature [K]

:math:`U` -internal energy [kJ mol\ :math:`^{-1}`]

:math:`W_w` -formula weight of water [kg kmol\ :math:`^{-1}`]

Greek Symbols
+++++++++++++

:math:`\varphi` -porosity [-]

:math:`\rho` -mass water density [kg m\ :math:`^{-3}`]

:math:`\eta` -molar water density [kmol m\ :math:`^{-3}`]

:math:`\mu` -viscosity [Pa s]

:math:`\Psi_j` -total concentration of :math:`j`\ th primary species [mol m\ :math:`^{-3}`]

:math:`\boldsymbol{\Omega}_j` -total flux of :math:`j`\ th primary species [mol m\ :math:`^{-2}` s\ :math:`^{-1}`]
