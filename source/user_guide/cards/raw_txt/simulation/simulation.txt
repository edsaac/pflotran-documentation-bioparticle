SKIP_RESTART
 On a restarted simulation, employs the initial condition for process model instead of the restarted solution.
