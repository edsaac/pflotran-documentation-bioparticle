.. _mac-install:

Mac Installation Instructions
=============================

Required software packages:
---------------------------
* Fortran 2003 compiler: gfortran > 4.7.x, intel >= 14
* Git_ source control management tool
* Message Passing Interface (MPI):  E.g.  `MPICH2 <http://www.mcs.anl.gov/research/projects/mpich2>`_, `Open MPI <http://www.open-mpi.org>`_, etc.
* BLAS/LAPACK libraries 
* Hierarchical Data Format HDF5_
* `PETSc git repository <https://gitlab.com/petsc/petsc>`_
* `METIS/ParMETIS <http://glaros.dtc.umn.edu/gkhome/metis/parmetis/overview>`_ (for unstructured grids)

Installation Instructions
-------------------------

To install PFLOTRAN on Mac, please follow 
the :ref:`Linux installation instructions <linux-install>`.

.. _Git: http://git-scm.com/
.. _PETSc: https://gitlab.com/petsc/petsc
.. _PETSc environment variables: http://www.mcs.anl.gov/petsc/documentation/installation.html#vars
.. _HDF5: http://www.hdfgroup.org/HDF5
.. _Bitbucket: https://bitbucket.org/pflotran/pflotran/wiki/Home.
