.. _previous-petsc-releases:

Previous PETSc Releases
=======================

2020-04-08 ~ Current: git checkout v3.13

2019-08-19 ~ 2020-04-08 (Hammond): git checkout v3.11.3

2018-10-10 ~ 2019-08-19 (Hammond): git checkout v3.10.2

2017-03-29 ~ 2018-10-10 (Hammond): git checkout xsdk-0.2.0

2016-09-06 ~ 2017-03-29 (Hammond): git checkout 1a9d3c3c50abf60098813fdf7291fe3540415115

2016-06-14 ~ 2016-09-06 (Hammond): git checkout 9fc87aa74b00c10f6fbaa6e6828460251b027710

2016-01-04 ~ 2016-06-14 (Hammond): git checkout 8c7fbf4f8491a74970a1205819563feba7a8e746

2015-11-04 ~ 2016-01-04 (Hammond): git checkout 788da0c1281918a2268034e28cedd2e44b0e70cf  

2014-12-30 ~ 2015-11-04 (Hammond): git checkout c41c7662de68b036bda6be236f939e8b55959cb0

2014-09-02 ~ 2014-12-30 (Hammond): git checkout c41c7662de68b036bda6be236f939e8b55959cb0

2014-04-17 ~ 2014-09-02 (Hammond): git checkout 821a7925fede8aa3b3b482fc9ccb2d087e2f80fa

2014-03-29 ~ 2014-04-17 (Hammond): git checkout 0c8c14c5

2014-01-14 ~ 2014-03-29 (Andre): git checkout 4663132a

Project Start ~ 2014-01-14 (Andre): git checkout 06283fd4
